# oidc_debug

Small script to debug an issue I encountered when using OIDC for rclone in a scipt. 

# prerequesites
At the top of the script two things are configured. First oidc-token needs to be able to generate tokens. In the script we assume the ESCAPE IAM instance has been configured to be mapped to the short name `ESCAPE` so that rwequesting a token can be done using `oidc-token ESCAPE`. Second there should be a webdav endpoint to copy the data to using `rclone`. This webdav endpoint is called `DESY-WEBDAV` in this script. The rclone configuration of this end point looks as follows in our setup:
```
[DESY-WEBDAV]
type = webdav
url = https://dcache-door-doma01.desy.de:2443/escape/wp2_rucio_testbed/desy_dcache_ndr/LOFAR_ASTRON_GRANGE/
vendor = other
bearer_token_command = oidc-token ESCAPE
```
(feel free to use the LOFAR_ASTRON_GRANGE directory for testing; or configure your own).

# Usage
The script has two ways of running. One that fails and one that succeeds. To run the script so that it fails simply execute
```
./test_oidc.sh
```
and you will see the different rclones returning several 401 errors.
To run a versiom that does not crash execute
```
./test_oidc.sh -f
```
The only thing this does is do an `rclone ls ${webdav_endpoint}:` before anything else. This should not necessarily change anything but still it makes it work...
