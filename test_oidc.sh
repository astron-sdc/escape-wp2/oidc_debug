#!/usr/bin/env bash

function cleanup() {
   rm *.otest.tmp     
}

# configuration:
export webdav_endpoint=DESY-WEBDAV
oidc_source=ESCAPE

eval `oidc-agent`
oidc-add ${oidc_source}

while getopts f flag
do
   case "${flag}" in
     f) rclone ls ${webdav_endpoint}:
   esac
done

# Rucio can refresh its token, but this is not working 
# so fro now I go for the poor-mans solution. This basically
# only refreshes the token if it has expired in the last 60 seconds
# This is far from ideal and could lead to a race condition
# if one happens to use rucio in the part of the 60 seconds that
# the token has expired. 
function refreshtoken {
   while true
   do
    oidc-token ESCAPE --aud=rucio --scope "openid profile offline_access" | tr -d '\n' > /tmp/grange/.rucio_grange/auth_token_for_account_grange
    sleep 60
   done
}

refreshtoken &
tokpid=$!

# want to run this set of commands usingGNU parallel, 
# so we wrap it in a function
function do_demo {
   testfilename=$(uuidgen)
   dd if=/dev/urandom of=${testfilename}.otest.tmp  bs=20480 count=10240
   rclone copy ${testfilename}.otest.tmp ${webdav_endpoint}:testdir
}

export uploaddir
export -f do_demo

# Do a bunch of copiues
for i in `seq 10`
do
 do_demo
done

cleanup

kill $tokpid
wait $tokpid 2>/dev/null
